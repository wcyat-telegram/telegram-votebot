import os
from os.path import exists


def createfile(filename):
    if checkexist(filename):
        os.remove(filename)
    fp = open(filename, "w")
    fp.close()


def checkexist(filename):
    r = bool(exists(filename))
    return r


def appendfile(filename, content):
    if checkexist(filename):
        fp = open(filename, "a")
        fp.write(content)
        fp.close()
    else:
        fp = open(filename, "w")
        fp.write(content)
        fp.close()


def authorised(user_id):
    r = False
    if checkexist("authorised_users"):
        file = open("authorised_users")
        for line in file:
            if str(user_id) in line:
                r = True
                break
        file.close()
    return r
